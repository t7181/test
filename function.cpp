#include"head.h"

void menu() {
	int flag;

	cout << "========================" << endl;
	cout << "电 话 号 码 查 找 系 统" << endl;
	cout << "========================" << endl;
	cout << "1.增加用户   2.删除用户" << endl;
	cout << "3.查找用户   4.修改用户" << endl;
	cout << "5.查看电话簿(电话号）" << endl;
	cout << "6.查看电话簿(姓名）" << endl;
	cout << "7.查看电话簿(电话号链表）" << endl;
	cout << "7.查看电话簿(姓名链表）" << endl;
	cout << "========================" << endl;
	cout << "\n" << endl;
	while (true) {
		cout << "请输出想要执行的功能的序号：";
		cin >> flag;
		switch (flag)
		{
		case 1: addUser(); break;
		case 2: deleteUser(); break;
		case 3: findUser(); break;
		case 4: updateUser(); break;
		case 5: showTelephoneDirectory('p'); break;
		case 6: showTelephoneDirectory('n'); break;
		case 7: showList('p'); break;
		case 8: showList('n'); break;
		default: cout << "输入错误，请重新输入：";
			break;
		}
	}
}	   //菜单

void addUser() {							//添加用户
	Phone phone[PhoneLength];
	Name name[NameLength];
	Address address[AddressLength];

	printf("请输入姓名："); cin >> name;
	printf("请输入手机号："); cin >> phone;
	printf("请输入地址："); cin >> address;



	int flag1 = 1;
	int key1 = creatMethod(phone);
	ChainAddressMethod(key1,name, phone, address);
	int temp1 = key1;
	while (flag1) {
		
		if (strlen(TelephoneDirectory_Phone[temp1].phone) == 0) {
			strncpy_s(TelephoneDirectory_Phone[temp1].phone, phone, 20);
			strncpy_s(TelephoneDirectory_Phone[temp1].name, name, 20);
			strncpy_s(TelephoneDirectory_Phone[temp1].address, address, 100);
			flag1 = 0;
		}
		else {
			temp1 = OpenAddressMethod(key1)%HashTableLength;
		}
	}
	di = 0;
	int flag2 = 1;
	int key2 = creatMethod(name);
	//ChainAddressMethod(key2, name, phone, address);
	int temp2 = key2;
	while (flag2) {

		if (strlen(TelephoneDirectory_Name[temp2].name) == 0) {
			strncpy_s(TelephoneDirectory_Name[temp2].phone, phone, 20);
			strncpy_s(TelephoneDirectory_Name[temp2].name, name, 20);
			strncpy_s(TelephoneDirectory_Name[temp2].address, address, 100);
			flag2 = 0;
		}
		else {
			temp2 = OpenAddressMethod(key2) % HashTableLength;
		}
	}
	di = 0;												//添加完成，将冲突次数置0
	if (flag1 == 0){
		cout << "添加成功！" << endl;
	}
}
void deleteUser() {

}

void updateUser() {

}

void findUser() {

}

void updateMethod() {

}

void showTelephoneDirectory(char flag) {					//显示电话簿
	if (flag == 'p'){
		cout << "姓名\t电话号\t地址\t(key)" << endl;
		for (int i = 0; i < HashTableLength; i++) {
			if (strlen(TelephoneDirectory_Phone[i].phone) != 0) {
				cout << TelephoneDirectory_Phone[i].name << "\t";
				cout << TelephoneDirectory_Phone[i].phone << "\t";
				cout << TelephoneDirectory_Phone[i].address << "\t";
				cout << i;
				cout << endl;
			}

		}
	}
	else if (flag == 'n') {
		cout << "姓名\t电话号\t地址\t(key)" << endl;
		for (int i = 0; i < HashTableLength; i++) {
			if (strlen(TelephoneDirectory_Name[i].phone) != 0) {
				cout << TelephoneDirectory_Name[i].name << "\t";
				cout << TelephoneDirectory_Name[i].phone << "\t";
				cout << TelephoneDirectory_Name[i].address << "\t";
				cout << i;
				cout << endl;
			}

		}
	}
}

int OpenAddressMethod(int key) {						// 开放地址法
	if (ResolveConflictsMethod == 1) {				// 线性探测法
		di++;										// di 初始化为0，没发生冲突一次，di+1
		return key + di;
	}
	if (ResolveConflictsMethod == 2) {				// 二次探测法
		di += 0.5;
		int result = pow(ceil(di),2);				// di = 1^2 , -1^2 , 2^2 , -2^2......
		if ((int)(di / 0.5) % 2 == 1) {				// di 初始化为零，每次加0.5 ，向上取整 ，从而实现 1，1，2，2，3，3，4，4....
			return key + result;					// 根据 di对0.5取余，判断当前是第几次冲突，从而判断正负，奇次为正，偶次为负
		}
		else {
			return key - result;
		}
	}
	if (ResolveConflictsMethod == 3) {				//伪随机探测法
		int randomNumber = (int)(rand() % 100);		//生成0—100的随机数
		return key + randomNumber;
	}
}

void ChainAddressMethod(int key, char name[], char phone[], char address[]) {						//链地址法
	LinkList p1 = TelephoneDirectory_PhoneList[key];
	LinkList temp1 = new HashTable;
	while (p1->next) {
		p1 = p1->next;
	}
	strncpy_s(temp1->phone, phone, 20);
	strncpy_s(temp1->name, name, 20);
	strncpy_s(temp1->address, address, 20);
	p1->next = temp1;
	temp1->next = NULL;
	cout << "OK" << endl;
}
		
void showList(char flag) {									//显示电话链表
	cout << "姓名\t电话号\t地址\t(key)" << endl;	
	if (flag == 'p') {
		for (int i = 0; i < HashTableLength; i++) {
			LinkList p = TelephoneDirectory_PhoneList[i]->next;
			while (p) {
				cout << p->name << "\t" << p->phone << "\t" << p->address << "\t" << i << endl;
				p = p->next;

			}
		}
	}
	else if (flag == 'n') {
		for (int i = 0; i < HashTableLength; i++) {
			LinkList p = TelephoneDirectory_NameList[i]->next;
			while (p) {
				cout << p->name << "\t" << p->phone << "\t" << p->address << "\t" << i << endl;
				p = p->next;

			}
		}
	}
}
void CreatHashTableList() {								//初始化电话链表和用户链表
	for (int i = 0; i < HashTableLength; i++) {
		InitList(TelephoneDirectory_PhoneList[i]);
		InitList(TelephoneDirectory_NameList[i]);
	}
}
void InitList(LinkList& L) {							//初始化链表
	L = new HashTable;
	L->next = NULL;
}

int creatMethod(char temp[]) {							//构造方法
	int key = 0;										//将字符数组每一位相加作为key值
	for (int i = 0; i < strlen(temp); i++) {
		key += temp[i];
	}
	int result = key % p;								//保留余数法
	return result;
}